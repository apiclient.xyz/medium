import { MediumAccount } from './medium.classes.mediumaccount';
import * as plugins from './medium.plugins';

export interface IMediumPublication {
  id: string;
  name: string;
  description: string;
  url: string;
  imageUrl: string;
}

export class MediumPublication implements IMediumPublication {
  // STATIC
  public static async getAllPublications(mediumAccount: MediumAccount) {
    await mediumAccount.readyDeferred.promise;
    const returnArray: MediumPublication[] = [];
    const response = await mediumAccount.request(`/users/${mediumAccount.id}/publications`, 'GET');
    const publicationsDataArray: IMediumPublication[] = response.body.data;
    for (const publicationData of publicationsDataArray) {
      const publication = new MediumPublication(mediumAccount, publicationData);
      returnArray.push(publication);
    }
    return returnArray;
  }

  public static async getOwnPublications(mediumAccount: MediumAccount) {
    await mediumAccount.readyDeferred.promise;
    const allPublications = await this.getAllPublications(mediumAccount);
    const ownPublications: MediumPublication[] = [];
    for (const publicationArg of allPublications) {
      const response = await mediumAccount.request(
        `/publications/${publicationArg.id}/contributors`,
        'GET'
      );
      const contributors: {
        publicationId: string;
        userId: string;
        role: string;
      }[] = response.body.data;
      for (const contributor of contributors) {
        if (contributor.userId === mediumAccount.id) {
          ownPublications.push(publicationArg);
          break;
        }
      }
    }
    return ownPublications;
  }

  // INSTANCE
  public mediumAccountRef: MediumAccount;

  id: string;
  name: string;
  description: string;
  url: string;
  imageUrl: string;

  constructor(mediumAccount: MediumAccount, dataArg: IMediumPublication) {
    Object.assign(this, dataArg);
  }
}
